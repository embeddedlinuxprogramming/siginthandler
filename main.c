//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include  <stdio.h>
#include  <signal.h>
#include  <stdlib.h>

void CtrlCHandler(int sig) {
    printf ("\x1B[31m-> Presionaste Ctrl + C. "
            "Saliendo del programa.\x1B[0m\n\r");
    exit(0);
}


int main(int argc, char* argv[]) {
    signal(SIGINT, CtrlCHandler);
    while (1)
        printf("Ciclo infinito.\n\r");
    return 0;
}
